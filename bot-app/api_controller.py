import requests
from v1 import auth_handler, bot_constants


class Api:
    def get_last_order(self, qr_id: str):
        return requests.get("http://127.0.0.1:8000/qr/order/" + qr_id, timeout=5)

    def get_qr_info(self, login: str, qr_id: str | None = None):
        jwt_token = auth_handler.sign_jwt(login=login)["access_token"]
        headers = {
            "accept": bot_constants.APPLICATION_JSON,
            "Authorization": bot_constants.BEARER + jwt_token,
            "Content-Type": bot_constants.APPLICATION_JSON,
        }
        if qr_id is None:
            return requests.get("http://127.0.0.1:8000/qr/", headers=headers, timeout=5)
        return requests.get("http://127.0.0.1:8000/qr/" + qr_id, timeout=5)

    def create_order(
        self, amount: float, qr_id: str, login: str, comment: str | None = None
    ):
        jwt_token = auth_handler.sign_jwt(login=login)["access_token"]
        if comment is None:
            headers = {
                "accept": bot_constants.APPLICATION_JSON,
                "Authorization": bot_constants.BEARER + jwt_token,
                "Content-Type": bot_constants.APPLICATION_JSON,
            }
            data = {"amount": amount, "qr": {"id": qr_id}}
            response = requests.post(
                "http://127.0.0.1:8000/order/create",
                headers=headers,
                json=data,
                timeout=5,
            )
        else:
            headers = {
                "accept": bot_constants.APPLICATION_JSON,
                "Authorization": bot_constants.BEARER + jwt_token,
                "Content-Type": bot_constants.APPLICATION_JSON,
            }
            data = {"amount": amount, "comment": comment, "qr": {"id": qr_id}}
            response = requests.post(
                "http://127.0.0.1:8000/order/create",
                headers=headers,
                json=data,
                timeout=5,
            )
        return response

    def get_order_info(self, login: str, order_id: str | None = None):
        jwt_token = auth_handler.sign_jwt(login=login)["access_token"]
        headers = {
            "accept": bot_constants.APPLICATION_JSON,
            "Authorization": bot_constants.BEARER + jwt_token,
            "Content-Type": bot_constants.APPLICATION_JSON,
        }
        if order_id is None:
            return requests.get(
                "http://127.0.0.1:8000/order/", headers=headers, timeout=5
            )
        return requests.get(
            "http://127.0.0.1:8000/order/" + order_id, headers=headers, timeout=5
        )

    def delete_order(self, login: str, order_id: str):
        jwt_token = auth_handler.sign_jwt(login=login)["access_token"]
        headers = {
            "accept": bot_constants.APPLICATION_JSON,
            "Authorization": bot_constants.BEARER + jwt_token,
            "Content-Type": bot_constants.APPLICATION_JSON,
        }
        return requests.delete(
            "http://127.0.0.1:8000/order/" + order_id, headers=headers, timeout=5
        )
