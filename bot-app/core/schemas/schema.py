from datetime import datetime
from typing import Optional
from uuid import UUID

from pydantic import BaseModel


class QrRequest(BaseModel):
    qrType: str
    account: Optional[str]
    sbpMerchantId: str
    redirectUrl: Optional[str]


class QrResponse(BaseModel):
    qrId: str
    qrStatus: str
    qrExpirationDate: Optional[str]
    payload: str
    qrUrl: str
    subscriptionId: Optional[str]

    class Config:
        orm_mode = True


class OrderExtras(BaseModel):
    apiClient: Optional[str]
    apiClientVersion: Optional[str]


class QrOrderInfo(BaseModel):
    id: str
    additionalInfo: Optional[str] = None
    paymentDetails: Optional[str] = None


class OrderStatus(BaseModel):
    value: Optional[str] = None
    date: Optional[str] = None


class OrderRequest(BaseModel):
    amount: float
    comment: Optional[str] = None
    extra: Optional[OrderExtras] = None
    expirationDate: Optional[str] = None
    qr: QrOrderInfo


class OrderResponse(BaseModel):
    id: str
    amount: float
    comment: Optional[str] = None
    extra: Optional[OrderExtras] = None
    status: OrderStatus
    expirationDate: Optional[str] = None
    qr: QrOrderInfo


class OrderReceiptResponse(OrderResponse):
    receiptNumber: Optional[UUID]


class OrderResponseWithDatetime(OrderReceiptResponse):
    last_time_update: datetime
    mobile_number: Optional[str]


class PaymentParameters(BaseModel):
    qrId: str


class Transaction(BaseModel):
    id: str
    orderId: str
    status: OrderStatus
    paymentMethod: str
    paymentParams: PaymentParameters
    amount: float
    comment: Optional[str] = None
    extra: Optional[OrderExtras]


class PaymentRequest(BaseModel):
    event: str
    transaction: Transaction


class PayeeCredentialsResponse(BaseModel):
    accout: str
    bic: str
    firstName: str
    middleName: str
    lastName: str
    phone: str


class CLientInformation(BaseModel):
    email: str
    name: Optional[str]

    class Config:
        orm_mode = True


class BillItem(BaseModel):
    name: str
    price: float
    quantity: float
    amount: float
    paymentObject: Optional[str]
    measurementUnit: Optional[str]
    nomenclatureCode: Optional[str]
    vatType: str
    agentType: Optional[str]

    class Config:
        orm_mode = True


class PaymentItem(BaseModel):
    type: str
    amount: float

    class Config:
        orm_mode = True


class BaseBill(BaseModel):
    items: list[BillItem]
    total: float

    class Config:
        orm_mode = True


class Bill(BaseBill):
    client: CLientInformation
    payments: Optional[list[PaymentItem]]


class RequestBill(Bill):
    receiptNumber: str


class ResponseBill(RequestBill):
    status: str
    receiptType: str


class RegRequest(BaseModel):
    login: str
    password: str
    chat_id: Optional[int]
    merchant_id: str
    token: str


class LoginRequest(BaseModel):
    login: str
    password: str
