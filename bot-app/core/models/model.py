from uuid import uuid4

from core.database import Base
from sqlalchemy import TIMESTAMP, BigInteger, Column, Float, ForeignKey, String
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func


class QrCodes(Base):
    __tablename__ = "qr_codes"

    id = Column(
        UUID(as_uuid=True),
        primary_key=True,
        index=True,
        nullable=False,
        default=uuid4,
        unique=True,
    )
    qrId = Column(String, nullable=False)
    qrStatus = Column(String, nullable=False)
    qrExpirationDate = Column(String, nullable=True)
    payload = Column(String, nullable=False)
    qrUrl = Column(String, nullable=False)
    subscriptionId = Column(String, nullable=True)
    userLogin = Column(String, ForeignKey("users.login"))


class Orders(Base):
    __tablename__ = "orders"

    orderId = Column(String, nullable=False, primary_key=True, index=True, unique=True)
    amount = Column(Float, nullable=False)
    status = Column(String, nullable=False)
    expirationDate = Column(String, nullable=True)
    qrId = Column(String, nullable=False)
    last_time_update = Column(
        TIMESTAMP, server_default=func.now(), onupdate=func.current_timestamp()
    )
    mobile_number = Column(String, nullable=True)
    userLogin = Column(String, ForeignKey("users.login"))


# class Suppliers(Base):
#     __tablename__ = "suppliers"
#     id = Column(
#         UUID(as_uuid=True),
#         primary_key=True,
#         index=True,
#         nullable=False,
#         default=uuid4,
#         unique=True,
#     )
#     email = Column(String, nullable=False)
#     name = Column(String, nullable=True)
#     item = relationship("Items", back_populates="supplierInfo")


class Items(Base):
    __tablename__ = "items"
    id = Column(
        UUID(as_uuid=True),
        primary_key=True,
        index=True,
        nullable=False,
        default=uuid4,
        unique=True,
    )
    name = Column(String, nullable=False)
    price = Column(Float, nullable=False)
    quantity = Column(Float, nullable=False)
    amount = Column(Float, nullable=False)
    paymentObject = Column(String, nullable=True)
    measurementUnit = Column(String, nullable=True)
    nomenclatureCode = Column(String, nullable=True)
    vatType = Column(String, nullable=False)
    agentType = Column(String, nullable=True)
    owner = relationship("Bills", back_populates="items")
    owner_id = Column(UUID(as_uuid=True), ForeignKey("bills.receiptNumber"))


class Clients(Base):
    __tablename__ = "clients"

    id = Column(
        UUID(as_uuid=True),
        primary_key=True,
        index=True,
        nullable=False,
        default=uuid4,
        unique=True,
    )
    email = Column(String, nullable=False)
    name = Column(String, nullable=True)
    owner = relationship("Bills", back_populates="client")
    owner_id = Column(UUID(as_uuid=True), ForeignKey("bills.receiptNumber"))


class Payments(Base):
    __tablename__ = "payments"
    id = Column(
        UUID(as_uuid=True),
        primary_key=True,
        index=True,
        nullable=False,
        default=uuid4,
        unique=True,
    )
    type = Column(String, nullable=False)
    amount = Column(Float, nullable=False)
    owner = relationship("Bills", back_populates="payments")
    owner_id = Column(UUID(as_uuid=True), ForeignKey("bills.receiptNumber"))


class Bills(Base):
    __tablename__ = "bills"
    receiptNumber = Column(
        UUID(as_uuid=True),
        primary_key=True,
        index=True,
        nullable=False,
        default=uuid4,
        unique=True,
    )
    client = relationship("Clients", back_populates="owner", uselist=False)
    items = relationship("Items", back_populates="owner")
    payments = relationship("Payments", back_populates="owner")
    total = Column(Float, nullable=False)
    receiptType = Column(String, nullable=False, default="SELL")
    status = Column(String, nullable=False, default="NEW")


class BillsConenctor(Base):
    __tablename__ = "bills_orders_connector"

    id = Column(
        UUID(as_uuid=True),
        primary_key=True,
        index=True,
        nullable=False,
        default=uuid4,
        unique=True,
    )
    bill_id = Column(UUID(as_uuid=True), ForeignKey("bills.receiptNumber"))
    order_id = Column(String, ForeignKey("orders.orderId"))


class Users(Base):
    __tablename__ = "users"

    id = Column(
        UUID(as_uuid=True),
        primary_key=True,
        index=True,
        nullable=False,
        default=uuid4,
        unique=True,
    )
    login = Column(String, nullable=False, unique=True)
    password = Column(String, nullable=False)
    chat_id = Column(BigInteger, nullable=True, unique=True)
    merchant_id = Column(String, nullable=False, unique=True)
    token = Column(String, nullable=False, unique=True)
