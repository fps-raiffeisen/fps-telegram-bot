import json
import logging
import math
import os
import urllib.parse
from contextlib import contextmanager

import api_controller
import cv2
import telebot
from core.database import SessionLocal
from decouple import config
from telebot import types
from telebot.types import (InlineKeyboardButton, InlineKeyboardMarkup,
                           ReplyKeyboardMarkup)
from v1 import bot_constants, bot_messages, crud

API_TOKEN = config("BOT_TOKEN")
CLIENT = api_controller.Api()
TOKEN = config("BEARER_TOKEN")
bot = telebot.TeleBot(API_TOKEN)

logging.basicConfig(
    format="%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s",
    datefmt="%H:%M:%S",
    level=logging.INFO,
)

_LOGGER = logging.getLogger(__name__)


@contextmanager
def get_db():
    db_session = SessionLocal()
    try:
        yield db_session
    finally:
        db_session.close()


def read_qr_code(filename):
    try:
        img = cv2.imread(filename)  # pylint: disable=no-member
        detector = cv2.QRCodeDetector()  # pylint: disable=no-member
        value, _, _ = detector.detectAndDecode(img)
        if not value:
            return None
        print(value)
        return value
    except Exception as exception:  # pylint: disable=broad-except
        _LOGGER.info("EXCEPTION while reading QR code: %s", exception)
        return None


# Handle '/start' and '/help'
@bot.message_handler(commands=["help"])
def ask_help(message: telebot.types.Message):
    _LOGGER.info("Help method at chat_id - %d", message.chat.id)
    bot.reply_to(
        message,
        bot_messages.HELP_MESSAGE,
        parse_mode="Markdown",
    )


def get_account(chat_id: int):
    with get_db() as db_session:
        user = crud.check_chat_id(db_session=db_session, chat_id=chat_id)
    return user


def start_menu(message: telebot.types.Message, bot_msg: str):
    _LOGGER.info("Start menu at chat_id - %d", message.chat.id)
    markup = ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)
    btn1 = types.KeyboardButton("Qr")
    btn2 = types.KeyboardButton("Заказ")
    markup.add(btn1, btn2)
    msg = bot.send_message(
        message.chat.id,
        bot_msg,
        parse_mode="Markdown",
        reply_markup=markup,
    )
    bot.register_next_step_handler(msg, select_category)


@bot.message_handler(commands=["start"])
def send_welcome(message: telebot.types.Message):
    user = get_account(message.chat.id)
    if user is not None:
        _LOGGER.info(
            "FIND ACCOUNT %s with chat_id %d", message.from_user.id, message.chat.id
        )
        start_menu(message, bot_messages.WELCOME)
    else:
        print("CANT FIND ACCOUNT with chat_id %d", message.chat.id)
        bot.send_message(
            message.chat.id,
            bot_messages.CANT_AUTHORISE.format(message.chat.id),
            parse_mode="Markdown",
        )


def select_category(message: telebot.types.Message):
    if message.text == "Qr":
        qr_category(message)
    elif message.text == "Заказ":
        order_category(message)


def order_category(message: telebot.types.Message):
    markup = ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)
    btn1 = types.KeyboardButton("Список всех заказов")
    btn2 = types.KeyboardButton("Id заказа")
    markup.add(btn1, btn2)
    msg = bot.send_message(
        message.chat.id,
        bot_messages.WELCOME,
        parse_mode="Markdown",
        reply_markup=markup,
    )
    bot.register_next_step_handler(msg, select_find_order)


def qr_category(message: telebot.types.Message):
    markup = ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)
    btn1 = types.KeyboardButton("Список Id")
    btn2 = types.KeyboardButton("Id")
    btn3 = types.KeyboardButton("Фото")
    markup.add(btn1, btn2, btn3)
    msg = bot.send_message(
        message.chat.id,
        bot_messages.WELCOME,
        parse_mode="Markdown",
        reply_markup=markup,
    )
    bot.register_next_step_handler(msg, select_find_qr)


def select_find_order(message: telebot.types.Message):
    if message.text == "Список всех заказов":
        orders_pagination(message)
    elif message.text == "Id заказа":
        request_certain_order_id(message)


def request_certain_order_id(message: telebot.types.Message):
    markup = ReplyKeyboardMarkup(resize_keyboard=True)
    btn1 = types.KeyboardButton(bot_constants.CANCEL)
    markup.add(btn1)
    msg = bot.send_message(
        message.chat.id,
        bot_messages.SEND_CERTAIN_ORDER_ID,
        parse_mode="Markdown",
        reply_markup=markup,
    )
    bot.register_next_step_handler(msg, get_certain_order)


def get_certain_order(message: telebot.types.Message):
    if not message.text:
        msg = bot.send_message(
            message.chat.id, bot_messages.RECEIVED_NOT_ORDER_ID, parse_mode="Markdown"
        )
        bot.register_next_step_handler(msg, get_certain_order)
        return
    if message.text == bot_constants.CANCEL:
        _LOGGER.info("Canceled certain order at chat_id - %d", message.chat.id)
        bot.send_message(
            message.chat.id,
            bot_constants.CANCELED,
            parse_mode="Markdown",
            reply_markup=telebot.types.ReplyKeyboardRemove(),
        )
        return
    user = get_account(message.chat.id)
    response = CLIENT.get_order_info(user.login, message.text)
    if "id" in response.json():
        _LOGGER.info("Find Order %s", message.text)
        markup = ReplyKeyboardMarkup(resize_keyboard=True)
        btn1 = types.KeyboardButton(bot_constants.DELETE_ORDER)
        btn2 = types.KeyboardButton(bot_constants.ORDER_INFO)
        btn3 = types.KeyboardButton(bot_constants.CANCEL)
        markup.add(btn1, btn2, btn3)
        msg = bot.send_message(
            message.chat.id,
            bot_messages.SELECT_FUNC_FOR_ORDER,
            parse_mode="Markdown",
            reply_markup=markup,
        )
        bot.register_next_step_handler(msg, select_function_order, message.text)
    else:
        _LOGGER.info("Order not found")
        markup = ReplyKeyboardMarkup(resize_keyboard=True)
        btn1 = types.KeyboardButton(bot_constants.CANCEL)
        markup.add(btn1)
        msg = bot.send_message(
            message.chat.id,
            bot_messages.ORDER_NOT_FOUND,
            parse_mode="Markdown",
            reply_markup=markup,
        )
        bot.register_next_step_handler(msg, get_certain_order)


def select_function_order(message: telebot.types.Message, order_id):
    print(order_id)
    if message.text == bot_constants.CANCEL:
        bot.send_message(
            message.chat.id,
            bot_constants.CANCELED,
            parse_mode="Markdown",
            reply_markup=telebot.types.ReplyKeyboardRemove(),
        )
        _LOGGER.info("Cancel function order at chat_id - %d", message.chat.id)
        start_menu(message, bot_messages.START_MENU)
    elif message.text == bot_constants.DELETE_ORDER:
        delete_order(message, order_id)
    elif message.text == bot_constants.ORDER_INFO:
        order_info(message, order_id)


def select_find_qr(message: telebot.types.Message):
    if message.text == "Список Id":
        send_qrs(message)
    elif message.text == "Id":
        request_certain_qr_id(message)
    elif message.text == "Фото":
        find_qr_by_photo(message)


def parse_qr_info(qrs: dict) -> str:
    parse_string = ""
    for key in qrs:
        parse_string += key + ": " + str(qrs[key]) + "\n"
    return parse_string


def request_certain_qr_id(message: telebot.types.Message):
    markup = ReplyKeyboardMarkup(resize_keyboard=True)
    btn1 = types.KeyboardButton(bot_constants.CANCEL)
    markup.add(btn1)
    msg = bot.send_message(
        message.chat.id,
        bot_messages.SEND_CERTAIN_QR_ID,
        parse_mode="Markdown",
        reply_markup=markup,
    )
    bot.register_next_step_handler(msg, get_certain_qr)


def get_certain_qr(message: telebot.types.Message):
    if not message.text:
        msg = bot.send_message(
            message.chat.id, bot_messages.RECEIVED_NOT_QR_ID, parse_mode="Markdown"
        )
        bot.register_next_step_handler(msg, get_certain_qr)
        return
    if message.text == bot_constants.CANCEL:
        bot.send_message(
            message.chat.id,
            bot_constants.CANCELED,
            parse_mode="Markdown",
            reply_markup=telebot.types.ReplyKeyboardRemove(),
        )
        _LOGGER.info("Canceled certain QR id at chat_id - %d", message.chat.id)
        start_menu(message, bot_messages.START_MENU)
        return
    user = get_account(message.chat.id)
    response = CLIENT.get_qr_info(user.login, message.text)
    if "qrId" in response.json():
        _LOGGER.info("Find Qr %d", message.text)
        markup = ReplyKeyboardMarkup(resize_keyboard=True)
        btn1 = types.KeyboardButton(bot_constants.LAST_ORDER_INFO)
        btn2 = types.KeyboardButton(bot_constants.QR_INFO)
        btn3 = types.KeyboardButton(bot_constants.CREATE_ORDER)
        btn4 = types.KeyboardButton(bot_constants.CANCEL)
        markup.add(btn1, btn2, btn3, btn4)
        msg = bot.send_message(
            message.chat.id,
            bot_messages.SELECT_FUNC_FOR_QR,
            parse_mode="Markdown",
            reply_markup=markup,
        )
        bot.register_next_step_handler(msg, select_function_qr, message.text)
    else:
        _LOGGER.info("QR not found")
        markup = ReplyKeyboardMarkup(resize_keyboard=True)
        btn1 = types.KeyboardButton(bot_constants.CANCEL)
        markup.add(btn1)
        msg = bot.send_message(
            message.chat.id,
            bot_messages.QR_NOT_FOUND,
            parse_mode="Markdown",
            reply_markup=markup,
        )
        bot.register_next_step_handler(msg, get_certain_qr)


def find_qr_by_photo(message: telebot.types.Message):
    markup = ReplyKeyboardMarkup(resize_keyboard=True)
    btn1 = types.KeyboardButton(bot_constants.CANCEL)
    markup.add(btn1)
    msg = bot.send_message(
        message.chat.id,
        bot_messages.PLEASE_SEND_QR,
        parse_mode="Markdown",
        reply_markup=markup,
    )
    bot.register_next_step_handler(msg, check_qr_photo)


def check_qr_photo(message: telebot.types.Message):
    if message.text == bot_constants.CANCEL:
        bot.send_message(
            message.chat.id,
            bot_constants.CANCELED,
            parse_mode="Markdown",
            reply_markup=telebot.types.ReplyKeyboardRemove(),
        )
        start_menu(message, bot_messages.START_MENU)
        return
    if not message.photo:
        msg = bot.send_message(
            message.chat.id, bot_messages.RECEIVED_NOT_QR, parse_mode="Markdown"
        )
        bot.register_next_step_handler(msg, check_qr_photo)
    photo_id = message.photo[-1].file_id
    name = "bot-app/photo/" + photo_id + ".jpg"
    photo_file = bot.get_file(photo_id)
    downloaded_file = bot.download_file(photo_file.file_path)
    with open(name, "wb") as photo:
        photo.write(downloaded_file)
    qr_link = read_qr_code(name)
    os.remove(name)
    if qr_link is None:
        _LOGGER.info("Not valid Qr")
        msg = bot.send_message(
            message.chat.id, bot_messages.NOT_VALID_QR, parse_mode="Markdown"
        )
        bot.register_next_step_handler(msg, check_qr_photo)
        return
    fragment = urllib.parse.urlparse(qr_link).path
    _LOGGER.info("Find Qr")
    markup = ReplyKeyboardMarkup(resize_keyboard=True)
    btn1 = types.KeyboardButton(bot_constants.LAST_ORDER_INFO)
    btn2 = types.KeyboardButton(bot_constants.QR_INFO)
    btn3 = types.KeyboardButton(bot_constants.CREATE_ORDER)
    btn4 = types.KeyboardButton(bot_constants.CANCEL)
    markup.add(btn1, btn2, btn3, btn4)
    msg = bot.send_message(
        message.chat.id,
        bot_messages.SELECT_FUNC_FOR_QR,
        parse_mode="Markdown",
        reply_markup=markup,
    )
    bot.register_next_step_handler(msg, select_function_qr, fragment[1:])


def select_function_qr(message: telebot.types.Message, qr_id):
    print(qr_id)
    if message.text == bot_constants.CANCEL:
        bot.send_message(
            message.chat.id,
            bot_constants.CANCELED,
            parse_mode="Markdown",
            reply_markup=telebot.types.ReplyKeyboardRemove(),
        )
        start_menu(message, bot_messages.START_MENU)
    elif message.text == bot_constants.LAST_ORDER_INFO:
        last_order(message, qr_id)
    elif message.text == bot_constants.QR_INFO:
        qr_info(message, qr_id)
    elif message.text == bot_constants.CREATE_ORDER:
        markup = ReplyKeyboardMarkup(resize_keyboard=True)
        btn1 = types.KeyboardButton(bot_constants.CANCEL)
        markup.add(btn1)
        msg = bot.send_message(
            message.chat.id,
            bot_messages.SELECT_AMOUNT,
            parse_mode="Markdown",
            reply_markup=markup,
        )
        bot.register_next_step_handler(msg, create_order, qr_id)


def last_order(message: telebot.types.Message, qr_id):
    response = CLIENT.get_last_order(qr_id)
    print(response.json())
    if "id" in response.json():
        bot.send_message(
            message.chat.id,
            bot_messages.LAST_ORDER_INFO.format(
                response.json()["id"],
                response.json()["qr"]["id"],
                response.json()["amount"],
                response.json()["status"]["value"],
                response.json()["expirationDate"],
            ),
            parse_mode="Markdown",
            reply_markup=telebot.types.ReplyKeyboardRemove(),
        )
    else:
        _LOGGER.info(
            "Cant find last order at qr_id - %d at chat_id - %d", qr_id, message.chat.id
        )
        bot.send_message(
            message.chat.id,
            bot_messages.CANT_FIND_ORDER,
            parse_mode="Markdown",
            reply_markup=telebot.types.ReplyKeyboardRemove(),
        )
    start_menu(message, bot_messages.START_MENU)


def qr_info(message: telebot.types.Message, qr_id):
    user = get_account(message.chat.id)
    response = CLIENT.get_qr_info(user.login, qr_id)
    print(response.json())
    if "qrId" in response.json():
        bot.send_message(
            message.chat.id,
            bot_messages.QR_INFO.format(
                response.json()["qrId"],
                response.json()["qrStatus"],
                response.json()["qrExpirationDate"],
            ),
            parse_mode="Markdown",
            reply_markup=telebot.types.ReplyKeyboardRemove(),
        )
    start_menu(message, bot_messages.START_MENU)


def delete_order(message: telebot.types.Message, order_id):
    user = get_account(message.chat.id)
    response = CLIENT.delete_order(user.login, order_id)
    print(response.status_code)
    if response.status_code == 200:
        bot.send_message(
            message.chat.id,
            bot_messages.SUCCESS_DELETE,
            parse_mode="Markdown",
            reply_markup=telebot.types.ReplyKeyboardRemove(),
        )
    else:
        bot.send_message(
            message.chat.id,
            bot_messages.FAULT_DELETE,
            parse_mode="Markdown",
            reply_markup=telebot.types.ReplyKeyboardRemove(),
        )
    start_menu(message, bot_messages.START_MENU)


def order_info(message: telebot.types.Message, order_id):
    user = get_account(message.chat.id)
    response = CLIENT.get_order_info(user.login, order_id)
    print(response.json())
    bot.send_message(
        message.chat.id,
        bot_messages.LAST_ORDER_INFO.format(
            response.json()["id"],
            response.json()["qr"]["id"],
            response.json()["amount"],
            response.json()["status"]["value"],
            response.json()["expirationDate"],
        ),
        parse_mode="Markdown",
        reply_markup=telebot.types.ReplyKeyboardRemove(),
    )
    start_menu(message, bot_messages.START_MENU)


def create_order(message: telebot.types.Message, qr_id):
    if not message.text:
        msg = bot.send_message(
            message.chat.id,
            bot_messages.INCORRECT_AMOUNT,
            parse_mode="Markdown",
            reply_markup=telebot.types.ReplyKeyboardRemove(),
        )
        bot.register_next_step_handler(msg, create_order, qr_id)
    if message.text == bot_constants.CANCEL:
        bot.send_message(
            message.chat.id,
            bot_constants.CANCELED,
            parse_mode="Markdown",
            reply_markup=telebot.types.ReplyKeyboardRemove(),
        )
        _LOGGER.info("Canceled create of order at chat_id - %d", message.chat.id)
    elif not message.text.isdigit():
        msg = bot.send_message(
            message.chat.id,
            bot_messages.INCORRECT_AMOUNT,
            parse_mode="Markdown",
            reply_markup=telebot.types.ReplyKeyboardRemove(),
        )
        bot.register_next_step_handler(msg, create_order, qr_id)
    else:
        amount = message.text
        markup = ReplyKeyboardMarkup(resize_keyboard=True)
        btn1 = types.KeyboardButton(bot_constants.CANCEL)
        btn2 = types.KeyboardButton("Без комментария")
        markup.add(btn1, btn2)
        msg = bot.send_message(
            message.chat.id,
            bot_messages.SELECT_COMMENT,
            parse_mode="Markdown",
            reply_markup=markup,
        )
        bot.register_next_step_handler(msg, select_comment, qr_id, amount)


def select_comment(message: telebot.types.Message, qr_id, amount):
    if not message.text:
        msg = bot.send_message(
            message.chat.id,
            bot_messages.INCORRECT_COMMENT,
            parse_mode="Markdown",
            reply_markup=telebot.types.ReplyKeyboardRemove(),
        )
        bot.register_next_step_handler(msg, select_comment, qr_id, amount)
        return
    if message.text == bot_constants.CANCEL:
        bot.send_message(
            message.chat.id,
            bot_constants.CANCELED,
            parse_mode="Markdown",
            reply_markup=telebot.types.ReplyKeyboardRemove(),
        )
    elif message.text == "Без комментария":
        user = get_account(message.chat.id)
        response = CLIENT.create_order(amount, qr_id, user.login)
        print(response.json())
        bot.send_message(
            message.chat.id,
            bot_messages.ORDER_RESPONSE.format(
                response.json()["id"],
                response.json()["amount"],
                "Без комментария",
                response.json()["status"]["value"],
                response.json()["expirationDate"],
                response.json()["qr"]["id"],
            ),
            parse_mode="Markdown",
            reply_markup=telebot.types.ReplyKeyboardRemove(),
        )
    else:
        user = get_account(message.chat.id)
        comment = message.text
        response = CLIENT.create_order(amount, qr_id, user.login, comment)
        print(response.json())
        bot.send_message(
            message.chat.id,
            bot_messages.ORDER_RESPONSE.format(
                response.json()["id"],
                response.json()["amount"],
                comment,
                response.json()["status"]["value"],
                response.json()["expirationDate"],
                response.json()["qr"]["id"],
            ),
            parse_mode="Markdown",
            reply_markup=telebot.types.ReplyKeyboardRemove(),
        )
    start_menu(message, bot_messages.START_MENU)


def orders_pagination(message: telebot.types.Message):
    user = get_account(message.chat.id)
    print(user.login)
    response = CLIENT.get_order_info(user.login)
    cnt_orders = len(response.json())
    print("COUNT OF PAGES - ", cnt_orders)
    page = 1
    markup = InlineKeyboardMarkup(row_width=2)
    markup.add(InlineKeyboardButton(text="Скрыть", callback_data="unseen"))
    print(response.json()[0]["id"])
    for i in range(min(4, cnt_orders - 4 * (page - 1))):
        markup.add(
            InlineKeyboardButton(
                text=response.json()[(4 * (page - 1)) + i]["id"],
                callback_data=bot_constants.ORDER_STATUS.format(
                    response.json()[(4 * (page - 1)) + i]["id"]
                ),
            )
        )
    markup.add(
        InlineKeyboardButton(
            text=f"{page}/{math.ceil(cnt_orders/4)}", callback_data=" "
        )
    )
    if cnt_orders > 4:
        markup.add(
            InlineKeyboardButton(
                text=bot_constants.FORWARD,
                callback_data=bot_constants.PAGINATION2.format(page + 1),
            ),
        )

    bot.send_message(
        message.from_user.id,
        "Выберите заказ из списка",
        reply_markup=markup,
    )


def send_qrs(message: telebot.types.Message):
    user = get_account(message.chat.id)
    print(user.login)
    response = CLIENT.get_qr_info(user.login)
    cnt_qrs = len(response.json())
    print("COUNT OF PAGES - ", cnt_qrs)
    page = 1
    markup = InlineKeyboardMarkup(row_width=2)
    markup.add(InlineKeyboardButton(text="Скрыть", callback_data="unseen"))
    for i in range(min(4, cnt_qrs - 4 * (page - 1))):
        markup.add(
            InlineKeyboardButton(
                text=response.json()[(4 * (page - 1)) + i]["qrId"],
                callback_data=bot_constants.QR_STATUS.format(
                    response.json()[(4 * (page - 1)) + i]["qrId"]
                ),
            )
        )
    markup.add(
        InlineKeyboardButton(text=f"{page}/{math.ceil(cnt_qrs/4)}", callback_data=" ")
    )
    if cnt_qrs > 4:
        markup.add(
            InlineKeyboardButton(
                text=bot_constants.FORWARD,
                callback_data=bot_constants.PAGINATION.format(page + 1),
            ),
        )

    bot.send_message(
        message.from_user.id,
        "Выберите QR код для просмотра статуса",
        reply_markup=markup,
    )


@bot.callback_query_handler(func=lambda call: True)
def callback_query(call):
    req = call.data.split("_")
    print(req)
    # Обработка кнопки - скрыть
    if "unseen" in req[0] or "cancel" in req[0]:
        bot.delete_message(call.message.chat.id, call.message.message_id)
    # Обработка кнопок - вперед и назад
    elif "pagination" in req[0]:
        # Расспарсим полученный JSON
        json_string = json.loads(req[0])
        user = get_account(call.message.chat.id)
        response = CLIENT.get_qr_info(user.login)
        cnt_qrs = len(response.json())
        count = math.ceil(cnt_qrs / 4)
        page = json_string["NumberPage"]
        # Пересоздаем markup
        markup = InlineKeyboardMarkup(row_width=2)
        markup.add(InlineKeyboardButton(text="Скрыть", callback_data="unseen"))
        for i in range(min(4, cnt_qrs - 4 * (page - 1))):
            markup.add(
                InlineKeyboardButton(
                    text=response.json()[(4 * (page - 1)) + i]["qrId"],
                    callback_data=bot_constants.QR_STATUS.format(
                        response.json()[(4 * (page - 1)) + i]["qrId"]
                    ),
                )
            )
        # markup для первой страницы
        if page == 1:
            markup.add(
                InlineKeyboardButton(text=f"{page}/{count}", callback_data=" "),
                InlineKeyboardButton(
                    text=bot_constants.FORWARD,
                    callback_data=bot_constants.PAGINATION.format(page + 1),
                ),
            )
        # markup для второй страницы
        elif page == count:
            markup.add(
                InlineKeyboardButton(
                    text=bot_constants.BACKWARD,
                    callback_data=bot_constants.PAGINATION.format(page - 1),
                ),
                InlineKeyboardButton(text=f"{page}/{count}", callback_data=" "),
            )
        # markup для остальных страниц
        else:
            markup.add(
                InlineKeyboardButton(
                    text=bot_constants.BACKWARD,
                    callback_data=bot_constants.PAGINATION.format(page - 1),
                ),
                InlineKeyboardButton(
                    text=bot_constants.FORWARD,
                    callback_data=bot_constants.PAGINATION.format(page + 1),
                ),
                InlineKeyboardButton(text=f"{page}/{count}", callback_data=" "),
            )
        bot.edit_message_text(
            f"Страница {page} из {count}",
            reply_markup=markup,
            chat_id=call.message.chat.id,
            message_id=call.message.message_id,
        )
    elif "NumberPage" in req[0]:
        # Расспарсим полученный JSON
        json_string = json.loads(req[0])
        user = get_account(call.message.chat.id)
        response = CLIENT.get_order_info(user.login)
        cnt_orders = len(response.json())
        count = math.ceil(cnt_orders / 4)
        page = json_string["NumberPage"]
        # Пересоздаем markup
        markup = InlineKeyboardMarkup(row_width=2)
        markup.add(InlineKeyboardButton(text="Скрыть", callback_data="unseen"))
        for i in range(min(4, cnt_orders - 4 * (page - 1))):
            markup.add(
                InlineKeyboardButton(
                    text=response.json()[(4 * (page - 1)) + i]["id"],
                    callback_data=bot_constants.ORDER_STATUS.format(
                        response.json()[(4 * (page - 1)) + i]["id"]
                    ),
                )
            )
        # markup для первой страницы
        if page == 1:
            markup.add(
                InlineKeyboardButton(text=f"{page}/{count}", callback_data=" "),
                InlineKeyboardButton(
                    text=bot_constants.FORWARD,
                    callback_data=bot_constants.PAGINATION2.format(page + 1),
                ),
            )
        # markup для второй страницы
        elif page == count:
            markup.add(
                InlineKeyboardButton(
                    text=bot_constants.BACKWARD,
                    callback_data=bot_constants.PAGINATION2.format(page - 1),
                ),
                InlineKeyboardButton(text=f"{page}/{count}", callback_data=" "),
            )
        # markup для остальных страниц
        else:
            markup.add(
                InlineKeyboardButton(
                    text=bot_constants.BACKWARD,
                    callback_data=bot_constants.PAGINATION2.format(page - 1),
                ),
                InlineKeyboardButton(
                    text=bot_constants.FORWARD,
                    callback_data=bot_constants.PAGINATION2.format(page + 1),
                ),
                InlineKeyboardButton(text=f"{page}/{count}", callback_data=" "),
            )
        bot.edit_message_text(
            f"Страница {page} из {count}",
            reply_markup=markup,
            chat_id=call.message.chat.id,
            message_id=call.message.message_id,
        )
    elif "QrStatus" in req[0]:
        json_string = json.loads(req[0])
        qr_id = json_string["QrId"]
        markup = ReplyKeyboardMarkup(resize_keyboard=True)
        btn1 = types.KeyboardButton(bot_constants.LAST_ORDER_INFO)
        btn2 = types.KeyboardButton(bot_constants.QR_INFO)
        btn3 = types.KeyboardButton(bot_constants.CREATE_ORDER)
        btn4 = types.KeyboardButton(bot_constants.CANCEL)
        markup.add(btn1, btn2, btn3, btn4)
        msg = bot.send_message(
            call.message.chat.id,
            bot_messages.SELECT_FUNC_FOR_QR,
            parse_mode="Markdown",
            reply_markup=markup,
        )
        bot.register_next_step_handler(msg, select_function_qr, qr_id)

    elif "OrderId" in req[0]:
        json_string = json.loads(req[0])
        order_id = json_string["OrderId"]
        markup = ReplyKeyboardMarkup(resize_keyboard=True)
        btn1 = types.KeyboardButton(bot_constants.DELETE_ORDER)
        btn2 = types.KeyboardButton(bot_constants.ORDER_INFO)
        btn3 = types.KeyboardButton(bot_constants.CANCEL)
        markup.add(btn1, btn2, btn3)
        msg = bot.send_message(
            call.message.chat.id,
            bot_messages.SELECT_FUNC_FOR_ORDER,
            parse_mode="Markdown",
            reply_markup=markup,
        )
        bot.register_next_step_handler(msg, select_function_order, order_id)


bot.infinity_polling(timeout=10, long_polling_timeout=5)
