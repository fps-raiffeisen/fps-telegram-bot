import time
from typing import Dict

import jwt
from decouple import config

JWT_SECRET = config("JWT_SECRET")


def token_response(token: str):
    return {"access_token": token}


def sign_jwt(login: str) -> Dict[str, str]:
    payload = {"login": login, "expires": time.time() + 3600}
    token = jwt.encode(payload, JWT_SECRET)

    return token_response(token)


def decode_jwt(token: str) -> dict:
    try:
        decoded_token = jwt.decode(token, JWT_SECRET, algorithms=["HS256"])
        return decoded_token if decoded_token["expires"] >= time.time() else {}
    except (
        jwt.exceptions.DecodeError,
        jwt.exceptions.InvalidTokenError,
    ):
        return {}
