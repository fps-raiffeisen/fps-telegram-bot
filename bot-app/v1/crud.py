from typing import Optional
from uuid import uuid4

from core.models import model
from core.schemas import schema
from sqlalchemy.orm import Session


def insert_qr(db_session: Session, response_order: schema.QrResponse, login: str):
    db_qr = model.QrCodes(
        qrId=response_order.qrId,
        qrStatus=response_order.qrStatus,
        qrExpirationDate=response_order.qrExpirationDate,
        payload=response_order.payload,
        qrUrl=response_order.qrUrl,
        subscriptionId=response_order.subscriptionId,
        userLogin=login,
    )
    db_session.add(db_qr)
    db_session.commit()
    db_session.refresh(db_qr)
    return db_qr


def get_qr(db_session: Session, qr_id: str):
    return db_session.query(model.QrCodes).filter(model.QrCodes.qrId == qr_id).first()


def get_qrs(
    db_session: Session,
    login: str,
    offset: Optional[int] = None,
    limit: Optional[int] = None,
):
    return list(
        db_session.query(model.QrCodes)
        .filter(model.QrCodes.userLogin == login)
        .offset(offset)
        .limit(limit)
    )


def insert_order(db_session: Session, response_order: schema.OrderResponse, login: str):
    db_order = model.Orders(
        orderId=response_order.id,
        amount=response_order.amount,
        status=response_order.status.value,
        expirationDate=response_order.expirationDate,
        qrId=response_order.qr.id,
        userLogin=login,
    )
    db_session.add(db_order)
    db_session.commit()
    db_session.refresh(db_order)
    return db_order


def get_order(db_session: Session, order_id: str):
    return (
        db_session.query(model.Orders).filter(model.Orders.orderId == order_id).first()
    )


def get_order_by_qr_id(db_session: Session, qr_id: str):
    return (
        db_session.query(model.Orders)
        .filter(model.Orders.qrId == qr_id)
        .order_by(model.Orders.expirationDate.desc())
        .first()
    )


def get_receipt_by_qr_id(db_session: Session, qr_id: str):
    order_id = (
        db_session.query(model.Orders)
        .filter(model.Orders.qrId == qr_id)
        .order_by(model.Orders.expirationDate.desc())
        .first()
    )

    if order_id:
        connector = (
            db_session.query(model.BillsConenctor)
            .filter(model.BillsConenctor.order_id == order_id.orderId)
            .first()
        )

        return connector.bill_id if connector else None
    return None


def get_receipt_by_order_id(db_session: Session, order_id: str):
    connector = (
        db_session.query(model.BillsConenctor)
        .filter(model.BillsConenctor.order_id == order_id)
        .first()
    )
    return connector.bill_id if connector else None


def get_qr_by_order_id(db_session: Session, order_id: str):
    return (
        db_session.query(model.Orders).filter(model.Orders.orderId == order_id).first()
    )


def get_orders(
    db_session: Session,
    login: str,
    offset: Optional[int] = None,
    limit: Optional[int] = None,
    phone_number: Optional[str] = None,
):
    if phone_number:
        return list(
            db_session.query(model.Orders)
            .filter(
                model.Orders.mobile_number == phone_number
                and model.Orders.userLogin == login
            )
            .offset(offset)
            .limit(limit)
        )
    return list(
        db_session.query(model.Orders)
        .filter(model.Orders.userLogin == login)
        .offset(offset)
        .limit(limit)
    )


def update_order_status(db_session: Session, order_id: str, new_status: str):
    db_order = get_order(db_session=db_session, order_id=order_id)
    db_order.status = new_status
    db_session.commit()
    db_session.refresh(db_order)
    return db_order


def update_qr_status(db_session: Session, qr_id: str, new_status: str):
    db_qr = get_qr(db_session=db_session, qr_id=qr_id)
    db_qr.qrStatus = new_status
    db_session.commit()
    db_session.refresh(db_qr)
    return db_qr


def save_bill(
    db_session: Session, order_id: str, bill: schema.BaseBill
) -> schema.RequestBill:
    client = schema.CLientInformation(email="warlockmikhail@gmail.com", name="John Doe")
    payments = [schema.PaymentItem(amount=bill.total, type="E_PAYMENT")]
    db_client = model.Clients(email=client.email, name=client.name)
    db_items = []

    for elem in bill.items:
        db_items.append(model.Items(**elem.dict()))
    db_payments = [model.Payments(amount=payments[0].amount, type=payments[0].type)]

    bill_id = str(uuid4())

    db_bill = model.Bills(
        receiptNumber=bill_id,
        client=db_client,
        items=db_items,
        payments=db_payments,
        total=bill.total,
    )

    # bill_one = schema.Bill()
    bill = schema.RequestBill(
        receiptNumber=bill_id,
        items=bill.items,
        total=bill.total,
        client=client,
        payments=payments,
    )

    db_session.add(db_bill)

    layer_connector = model.BillsConenctor(bill_id=bill_id, order_id=order_id)
    db_session.add(layer_connector)

    db_session.commit()

    db_session.refresh(db_bill)
    db_session.refresh(layer_connector)
    return bill


def connect_phone_number_with_order(
    db_session: Session, order_id: str, phone_number: str
):
    db_order = get_order(db_session=db_session, order_id=order_id)
    db_order.mobile_number = phone_number
    db_session.commit()
    db_session.refresh(db_order)
    return db_order


def registrate_user(db_session: Session, response_reg: schema.RegRequest):
    db_reg = model.Users(
        login=response_reg.login,
        password=response_reg.password,
        chat_id=response_reg.chat_id,
        merchant_id=response_reg.merchant_id,
        token=response_reg.token,
    )
    db_session.add(db_reg)
    db_session.commit()
    db_session.refresh(db_reg)
    return db_reg


def get_user(db_session: Session, login: str):
    return db_session.query(model.Users).filter(model.Users.login == login).first()


def get_user_by_token(db_session: Session, token: str):
    return db_session.query(model.Users).filter(model.Users.token == token).first()


def add_tg_account(db_session: Session, chat_id: str, login: str):
    user = get_user(db_session=db_session, login=login)
    user.chat_id = chat_id
    db_session.commit()
    db_session.refresh(user)
    return user


def get_users(
    db_session: Session, offset: Optional[int] = None, limit: Optional[int] = None
):
    return list(db_session.query(model.Users).offset(offset).limit(limit))


def check_chat_id(db_session: Session, chat_id: int):
    return db_session.query(model.Users).filter(model.Users.chat_id == chat_id).first()
