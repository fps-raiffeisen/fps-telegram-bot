CANCEL = "Отмена"

CANCELED = "Отменено"

LAST_ORDER_INFO = "Информация о последнем заказе"

QR_INFO = "Информация о Qr коде"

CREATE_ORDER = "Создать заказ"

PAGINATION = "{{" + "\"method\":\"pagination\",\"NumberPage\": " + "{}" + "}}"

PAGINATION2 = "{{" + "\"NumberPage\": " + "{}" + "}}"

QR_STATUS = "{{\"method\":\"QrStatus\",\"QrId\":" + "\"" + "{}" + "\"" + "}}"

ORDER_STATUS = "{{\"OrderId\":" + "\"" + "{}" + "\"" + "}}"

FORWARD = "Вперёд ⏩"

BACKWARD = "⏪ Назад"

APPLICATION_JSON = "application/json"

BEARER = "Bearer "

DELETE_ORDER = "Отменить заказ"

ORDER_INFO = "Информация о заказе"
